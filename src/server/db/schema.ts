import { InferSelectModel, relations } from "drizzle-orm";
import { text, pgTableCreator, uuid, unique } from "drizzle-orm/pg-core";

/**
 * This is an example of how to use the multi-project schema feature of Drizzle ORM. Use the same
 * database instance for multiple projects.
 *
 * @see https://orm.drizzle.team/docs/goodies#multi-project-schema
 */

// Change name of copy_hub_t3 to create prefixes for tables:
export const pgTable = pgTableCreator((name) => `${name}`);

export const users = pgTable("user", {
  id: uuid("id").defaultRandom().primaryKey(),
  name: text("name"),
  password: text("password").notNull(),
});
export const students = pgTable("students", {
  id: uuid("id").defaultRandom().primaryKey(),
  name: text("name"),
  date_birth: text("date"),
  father: text("father"),
  phone: text("phone"),
  phone2: text("phone2").default(""),
  address: text("address").default(""),
  classId: uuid("classId")
    .notNull()
    .references(() => classs.id),
});

export const studentsRelations = relations(students, ({ many, one }) => ({
  attendances: many(attendances),
  class: one(classs, {
    fields: [students.classId],
    references: [classs.id],
  }),
}));

export const classs = pgTable("class", {
  id: uuid("id").defaultRandom().primaryKey(),
  name: text("name"),
});
export const classsRelations = relations(classs, ({ many }) => ({
  students: many(students),
}));

export const attendances = pgTable(
  "attendances",
  {
    id: uuid("id").defaultRandom().primaryKey(),
    date: text("date").notNull(),
    studentId: uuid("studentId")
      .notNull()
      .references(() => students.id),
  },
  (t) => ({
    i: unique().on(t.date, t.studentId),
  }),
);
export const attendancesRelations = relations(attendances, ({ one }) => ({
  attendances: one(students, {
    fields: [attendances.studentId],
    references: [students.id],
  }),
}));

export type Classs = InferSelectModel<typeof classs> & {
  students?: Student[];
  count?: number;
};
export type Attendances = InferSelectModel<typeof attendances>;
export type Student = InferSelectModel<typeof students> & {
  class?: Classs;
  attendances?: Attendances[] | string[];
};
