import { randomUUID } from "crypto";
import { NextApiRequest, NextApiResponse } from "next";
import { NextResponse } from "next/server";
import { db } from "~/server/db";
import { classs } from "~/server/db/schema";

export async function POST(req: Request) {
  const data = (await req.json()) as { name: string };

  await db.insert(classs).values({ id: randomUUID(), name: data.name });
  return NextResponse.json({
    message: "reCAPTCHA verification failed",
  });
}
export async function GET(req: Request) {
  const data = await db.select().from(classs);
  return NextResponse.json(data);
}
