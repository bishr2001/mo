/* eslint-disable @typescript-eslint/no-floating-promises */
"use client";
import "~/styles/globals.css";
import { Inter, Baloo_Bhaijaan_2 } from "next/font/google";
import { useEffect } from "react";
import { getSession } from "next-auth/react";
import { useRouter } from "next/navigation";

const Baloo = Baloo_Bhaijaan_2({
  subsets: ["arabic", "latin"],
  variable: "--font-sans",
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const router = useRouter();
  async function check() {
    const t = await getSession();
    console.log(t);
    if (t) router.push("/main");
    else router.push("/login");
  }
  useEffect(() => {
    check();
  }, []);

  return (
    <html lang="en" className="dark">
      <body className={`font-sans ${Baloo.variable}`}>{children}</body>
    </html>
  );
}
