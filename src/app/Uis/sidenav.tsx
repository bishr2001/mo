"use client";

import Link from "next/link";
import { cn } from "../lib/utils";

export default function Sidebar({ show }: { show: boolean }) {
  const items = [
    { name: "Students", link: "/main/students" },
    { name: "Class", link: "/main/class" },
    { name: "Attendance", link: "/main/attendance" },
    { name: "Scan", link: "/main/scan" },
    { name: "QR Code", link: "/main/qrs" },
  ];
  return (
    <aside className={cn(show ? "block" : "hidden")}>
      <ul className=" h-full rounded-xl bg-white bg-opacity-10 p-4">
        {items.map((item) => (
          <li
            className=" mb-2 cursor-pointer rounded-xl  bg-gold p-4 text-center text-lg font-bold text-white"
            key={item.name}
          >
            <Link href={item.link}>{item.name}</Link>
          </li>
        ))}
      </ul>
    </aside>
  );
}
