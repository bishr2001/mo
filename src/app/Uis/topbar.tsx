"use client";
import { FaAlignJustify } from "react-icons/fa6";
export default function Topbar({
  show,
  setShow,
}: {
  show: boolean;
  setShow: (arg0: boolean) => void;
}) {
  return (
    <header className="bg-white bg-opacity-10 p-2">
      <button
        onClick={() => {
          setShow(!show);
        }}
      >
        <FaAlignJustify className="text-2xl text-gold" />
      </button>
    </header>
  );
}
