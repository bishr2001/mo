import { eq } from "drizzle-orm";
import NextAuth from "next-auth/next";
import CredentialsProvider from "next-auth/providers/credentials";
import { db } from "~/server/db";
import { users } from "~/server/db/schema";
import { verify, hash } from "argon2";

// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
const handler = NextAuth({
  session: {
    strategy: "jwt",
  },

  pages: {
    signIn: "/login",
  },
  providers: [
    CredentialsProvider({
      name: "Credentials",

      credentials: {
        username: {},
        password: {},
      },
      async authorize(credentials, req) {
        const { username, password } = req.body as {
          username: string;
          password: string;
        };
        console.log(username);
        const d = await db.query.users.findFirst({
          where: eq(users.name, username),
        });

        console.log(d);

        if (!d) return null;

        const t = await verify(d.password, password);

        console.log("hash", t);

        if (t) {
          return {
            ...d,
            password: "",
          };
        }
        return null;
      },
    }),
  ],
});

export { handler as GET, handler as POST };
