import { sql } from "drizzle-orm";
import { revalidatePath } from "next/cache";
import { NextResponse } from "next/server";
import { db } from "~/server/db";
import { classs } from "~/server/db/schema";

export async function POST(req: Request) {
  const data = (await req.json()) as { name: string };

  await db.insert(classs).values({ name: data.name });
  revalidatePath("/main/class", "page");

  return NextResponse.json({
    message: "reCAPTCHA verification failed",
  });
}
export async function GET(req: Request) {
  const data = await db.query.classs.findMany({
    with: {
      students: true,
    },
    extras: {
      count: sql`(SELECT count(*) from students)`.as("count"),
    },
  });
  return NextResponse.json(data);
}
