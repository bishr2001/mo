import { randomUUID } from "crypto";
import { NextResponse } from "next/server";
import { db } from "~/server/db";
import { attendances } from "~/server/db/schema";

export async function POST(req: Request) {
  let data = (await req.json()) as {
    id: string;
    date: string;
    studentId: string;
  }[];
  data = data.map((v) => {
    return { ...v, studentId: v.id };
  });
  console.log(data);

  const s = await db.insert(attendances).values(data).onConflictDoNothing();
  return NextResponse.json({
    message: "reCAPTCHA verification failed",
    data: s,
  });
}
export async function GET(req: Request) {
  const data = await db.query.students.findMany({
    with: { attendances: true },
  });
  return NextResponse.json(data);
}
