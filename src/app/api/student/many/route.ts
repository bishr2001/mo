import { NextResponse } from "next/server";
import { db } from "~/server/db";
import { students } from "~/server/db/schema";

export async function POST(req: Request) {
  const data = (await req.json()) as {
    name: string;
    father: string;
    date_birth: string;
    phone: string;
    classId: string;
  }[];

  await db.insert(students).values([...data]);
  return NextResponse.json({
    message: "reCAPTCHA verification failed",
  });
}
