import { eq } from "drizzle-orm";
import { NextRequest, NextResponse } from "next/server";
import { db } from "~/server/db";
import { students } from "~/server/db/schema";

export async function DELETE(
  req: NextRequest,
  { params }: { params: { id: string } },
) {
  const data = await db.delete(students).where(eq(students.id, params.id));

  return NextResponse.json(data);
}

export async function GET(
  req: NextRequest,
  { params }: { params: { id: string } },
) {
  console.log(req);
  const data = await db
    .select()
    .from(students)
    .where(eq(students.id, params.id));
  return NextResponse.json(data);
}
