"use client";
import { useEffect, useState } from "react";

import dynamic from "next/dynamic";
import { Button } from "~/components/ui/button";
import { FaTrash } from "react-icons/fa";

const DynamicQrScanner = dynamic(
  () => import("@yudiel/react-qr-scanner").then((mod) => mod.Scanner),
  {
    ssr: false,
  },
);

export default function Scan() {
  const [data, setData] = useState<
    { name: string; id: string; date: string }[]
  >([{ name: "string", id: "string", date: "string" }]);

  useEffect(() => {
    const att: string =
      typeof window !== "undefined" ? localStorage.getItem("att")! : "[]";

    if (att) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      setData(JSON.parse(att));
    }
  }, []);

  const ScanQr = (text: string) => {
    const c: { name: string; id: string } = JSON.parse(text) as {
      name: string;
      id: string;
    };
    const d = data.find((val) => {
      return val.id == c.id;
    });
    if (!d) {
      const da = [...data, { ...c, date: new Date().toLocaleDateString() }];
      localStorage.setItem("att", JSON.stringify(da));
      setData([...data, { ...c, date: new Date().toLocaleDateString() }]);
    }
  };

  async function save() {
    const c = await fetch("/api/attendance", {
      method: "Post",
      body: JSON.stringify(data),
    });
    console.log(c);
    if (c.ok) {
      setData([]);
    }
  }

  return (
    <div className="">
      <DynamicQrScanner onResult={ScanQr} />

      <div className="flex justify-center p-3">
        <Button onClick={save}>save</Button>
      </div>
      <div className="  ">
        {data.map((val, index) => (
          <div
            className="my-2  w-full rounded-xl bg-white bg-opacity-20 p-4"
            key={val.id}
          >
            <div>name: {val.name}</div>
            <div>id: {val.id}</div>
            <div>date: {val.date}</div>
            <FaTrash
              onClick={() => {
                const d = [...data];
                d.splice(index, 1);
                setData(d);
              }}
            />
          </div>
        ))}
      </div>
    </div>
  );
}
