"use client";
import { Suspense, useState } from "react";
import Topbar from "../Uis/topbar";
import Sidebar from "../Uis/sidenav";

export default function MainLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const [show, setShow] = useState(false);
  return (
    <div className="  overflow-hidden  bg-gradient-to-tr from-bg1 to-bg2">
      <Topbar show={show} setShow={setShow} />
      <div className="flex min-h-[100svh]  overflow-auto  px-2 py-4">
        <Sidebar show={show}></Sidebar>
        <main className="mx-4 h-full w-full rounded-xl  border-2 border-white  border-opacity-10 p-4  text-white">
          <Suspense fallback={<div>loading</div>}>{children}</Suspense>
        </main>
      </div>
    </div>
  );
}
