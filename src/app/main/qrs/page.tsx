"use client";
import { useQRCode } from "next-qrcode";
import { createRef, useEffect, useState } from "react";
import { type Student } from "~/server/db/schema";
import html2canvas from "html2canvas";
import { Button } from "~/components/ui/button";
import { jsPDF } from "jspdf";
import { Input } from "~/components/ui/input";
import Image from "next/image";
import png from "/public/logo.png";

export default function Qr() {
  const { Canvas } = useQRCode();

  const [students, setStudents] = useState<Student[]>([]);

  const [filter, setFilter] = useState<string>("");

  const myRef = createRef<HTMLDivElement>();

  useEffect(() => {
    fetch(`/api/student`, { cache: "no-store" })
      .then((response) => response.json())
      .catch((e) => {
        console.log(e);
      })
      .then((json: Student[]) => {
        setStudents(json);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  async function saveCapture() {
    const elemente = myRef.current;
    const canvas = await html2canvas(elemente!);

    const imgWidth = 295;
    // const imgWidth = 210;
    const pageHeight = 210;
    // const pageHeight = 295;
    const imgHeight = (canvas.height * imgWidth) / canvas.width;
    let heightLeft = imgHeight;
    const doc = new jsPDF("l", "mm");
    let position = 0; // give some top padding to first page

    doc.addImage(canvas, "PNG", 0, position, imgWidth, imgHeight);
    heightLeft -= pageHeight;
    while (heightLeft >= 0) {
      position = heightLeft - imgHeight; // top padding for other pages
      doc.addPage();
      doc.addImage(canvas, "PNG", 0, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;
    }
    doc.save("file.pdf");
  }

  return (
    <div className="p-3">
      <div className="py-3">
        <Input
          placeholder="Filter By Name..."
          value={filter}
          onChange={(e) => {
            setFilter(e.target.value);
          }}
          className="max-w-sm"
          name="name"
        />
      </div>
      <div className=" grid grid-cols-3 bg-white" ref={myRef}>
        {students
          .filter((val) => {
            return val.name
              ?.toLocaleLowerCase()
              .includes(filter.toLocaleLowerCase());
          })
          .map((val) => (
            <div
              key={val.id}
              className="flex h-[221px]  border border-dashed border-black p-4
          "
            >
              <div className="w-1/2">
                <Canvas
                  text={JSON.stringify({ name: val.name, id: val.id })}
                  options={{
                    errorCorrectionLevel: "M",
                    margin: 3,
                    scale: 3,
                    width: 180,
                    color: {
                      dark: "#000",
                      light: "#fff",
                    },
                  }}
                />
              </div>
              <div className="flex  flex-wrap justify-center ">
                <Image src={png} width={90} height={60} alt="'logo'"></Image>

                <div className=" w-full text-center  text-lg text-black">
                  {val.name}
                </div>
                <div className=" w-full text-center  text-lg text-black">
                  {val.class?.name}
                </div>
              </div>
            </div>
          ))}
      </div>

      <Button
        onClick={async () => {
          await saveCapture();
        }}
      >
        {" "}
        save
      </Button>
    </div>
  );
}
