/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-explicit-any */
"use client";
import { useEffect, useState } from "react";
import { Attendances, Student } from "~/server/db/schema";
import DataTableDemo from "./table";

export default function Students() {
  const [students, setStudents] = useState<any[]>([]);
  const [columns, setColumns] = useState<any[]>([
    {
      accessorKey: "name",
      header: "Name",
    },
  ]);
  useEffect(() => {
    fetch(`/api/attendance`, { cache: "no-store" })
      .then((response) => response.json())
      .catch((e) => {
        console.log(e);
      })
      .then((json: Student[]) => {
        const dates = new Set<string>();
        let st: Student[] = JSON.parse(JSON.stringify(json)) as Student[];
        console.log(st);
        st = st
          .filter((v) => v)
          .map((val) => {
            val.attendances = (val.attendances as Attendances[]).map((v) => {
              dates.add(v.date);
              return v.date;
            });
            return val;
          });
        const d = [...dates];
        d.filter((v) => v).map((val) => {
          setColumns([
            ...columns,
            {
              accessorKey: "attendances",
              header: val,
            },
          ]);
        });
        const f: any[] = JSON.parse(JSON.stringify(st));
        f.map((val) => {
          val.attendances.map((v: any) => {
            val[v] = true;
          });
          return val;
        });
        setStudents(f);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  if (students.length == 0) {
    return <div>loading</div>;
  }
  return (
    <div>
      <h1>Attendance</h1>
      <div>
        <DataTableDemo data={students} columns={columns}></DataTableDemo>
      </div>
    </div>
  );
}
