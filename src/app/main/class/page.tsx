"use client";
import DataTableDemo from "./table";
import AddButton from "./addButton";
import { useEffect, useState } from "react";
import { Classs, Student } from "~/server/db/schema";

export default function Students() {
  const [data, setData] = useState<Classs[]>([]);
  useEffect(() => {
    fetch(`/api/class`, { cache: "no-store" })
      .then((response) => response.json())
      .catch((e) => {
        console.log(e);
      })
      .then((json: Classs[]) => {
        setData(json);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);
  return (
    <div>
      <h1>Class</h1>
      <AddButton />
      <div>
        <DataTableDemo data={data}></DataTableDemo>
      </div>
    </div>
  );
}
