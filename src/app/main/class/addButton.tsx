"use client";
import { useState } from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "../../../components/ui/alert-dialog";
import { Button } from "../../../components/ui/button";
import { Input } from "~/components/ui/input";

export default function AddButton() {
  return (
    <div>
      <AddForm></AddForm>
    </div>
  );
}

function AddForm() {
  const [inp, setInp] = useState("");
  return (
    <AlertDialog>
      <AlertDialogTrigger asChild>
        <Button variant="outline">Add Class</Button>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>
            <div className="text-white">Add Class</div>
          </AlertDialogTitle>
          <AlertDialogDescription>
            <div className="w-full p-4">
              <Input
                placeholder="Name"
                value={inp}
                onChange={(e) => {
                  setInp(e.target.value);
                }}
                className="w-full "
                name="name"
              />
            </div>
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction onClick={send}>Continue</AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
  async function send() {
    const c = await fetch("/api/class", {
      method: "Post",
      body: JSON.stringify({ name: inp }),
    });
    console.log(c);
  }
}
