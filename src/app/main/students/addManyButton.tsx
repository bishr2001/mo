"use client";
import { ChangeEvent, useEffect, useState } from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "../../../components/ui/alert-dialog";
import { Button } from "../../../components/ui/button";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "../../../components/ui/select";
import { Classs, Student } from "~/server/db/schema";

import * as XLSX from "xlsx";

export default function AddManyButton() {
  return (
    <div>
      <AddForm></AddForm>
    </div>
  );
}

function AddForm() {
  const [student, setStudent] = useState<Student[]>([]);

  const [classs, setClasss] = useState<Classs[]>([]);
  const [classId, setClassId] = useState<string>("");

  useEffect(() => {
    fetch(`/api/class`, { cache: "no-store" })
      .then((response) => response.json())
      .catch((e) => {
        console.log(e);
      })
      .then((json: Classs[]) => setClasss(json))
      .catch((e) => {
        console.log(e);
      });
  }, []);

  const fileToJson = (t: string | ArrayBuffer) => {
    const d = XLSX.read(t, { type: "binary" });
    const names = d.SheetNames[0]!;

    const all = d.Sheets[names]!;

    const data = XLSX.utils.sheet_to_json(all);
    return data;
  };

  const FileChange = (file: ChangeEvent<HTMLInputElement>) => {
    const reader = new FileReader();
    const t = file.target.files!.item(0)!;

    reader.readAsArrayBuffer(t);
    reader.onload = (e) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-asserted-optional-chain
      let data: Student[] = fileToJson(e.target?.result!) as Student[];
      data = data.map((v: Student) => {
        return { ...v, classId: classId };
      });
      setStudent(data);
    };
  };
  return (
    <AlertDialog>
      <AlertDialogTrigger asChild>
        <Button variant="outline">Add Many Student</Button>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>
            <div className="text-white">Add Many Student</div>
          </AlertDialogTitle>
          <AlertDialogDescription>
            <div className="p-3">
              <Select
                defaultValue={classId}
                onValueChange={(e) => {
                  setClassId(e);
                }}
              >
                <SelectTrigger className="my-3 w-full">
                  <SelectValue placeholder="Select a class" />
                </SelectTrigger>
                <SelectContent>
                  <SelectGroup>
                    {classs.map((c) => (
                      <SelectItem key={c.id} value={c.id} className="w-full">
                        {c.name}
                      </SelectItem>
                    ))}
                  </SelectGroup>
                </SelectContent>
              </Select>

              <input type="file" onChange={FileChange} />
            </div>
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction onClick={send}>Continue</AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
  async function send() {
    const s: Student[] = [...student];
    const c = await fetch("/api/student/many", {
      method: "Post",
      body: JSON.stringify(s),
    });
    console.log(c);
  }
}
