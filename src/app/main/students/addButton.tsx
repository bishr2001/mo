"use client";
import { useEffect, useState } from "react";
import {
  AlertDialog,
  AlertDialogAction,
  AlertDialogCancel,
  AlertDialogContent,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogTrigger,
} from "../../../components/ui/alert-dialog";
import { Button } from "../../../components/ui/button";
import { Input } from "~/components/ui/input";
import {
  Select,
  SelectContent,
  SelectGroup,
  SelectItem,
  SelectLabel,
  SelectTrigger,
  SelectValue,
} from "../../../components/ui/select";
import { Classs, Student } from "~/server/db/schema";

export default function AddButton() {
  return (
    <div>
      <AddForm></AddForm>
    </div>
  );
}

function AddForm() {
  const [student, setStudent] = useState<Student>({
    id: "",
    name: "",
    date_birth: "",
    father: "",
    phone: "",
    classId: "",
    phone2: "",
    address: "",
  });

  const [classs, setClasss] = useState<Classs[]>([]);

  useEffect(() => {
    fetch(`/api/class`, { cache: "no-store" })
      .then((response) => response.json())
      .catch((e) => {
        console.log(e);
      })
      .then((json: Classs[]) => setClasss(json))
      .catch((e) => {
        console.log(e);
      });
  }, []);
  return (
    <AlertDialog>
      <AlertDialogTrigger asChild>
        <Button variant="outline">Add Student</Button>
      </AlertDialogTrigger>
      <AlertDialogContent>
        <AlertDialogHeader>
          <AlertDialogTitle>
            <div className="text-white">Add Student</div>
          </AlertDialogTitle>
          <AlertDialogDescription>
            <div className="p-2">
              <Input
                placeholder="Name"
                value={student.name + ""}
                onChange={(e) => {
                  setStudent({ ...student, name: e.target.value });
                }}
                className="my-3 w-full"
                name="name"
              />
              <Input
                placeholder="father"
                value={student.father + ""}
                onChange={(e) => {
                  setStudent({ ...student, father: e.target.value });
                }}
                className="my-3 w-full"
                name="father"
              />
              <Input
                placeholder="phone"
                value={student.phone + ""}
                onChange={(e) => {
                  setStudent({ ...student, phone: e.target.value });
                }}
                className="my-3 w-full"
                name="phone"
              />
              <Input
                placeholder="phone2"
                value={student.phone2 + ""}
                onChange={(e) => {
                  setStudent({ ...student, phone2: e.target.value });
                }}
                className="my-3 w-full"
                name="phone2"
              />
              <Input
                placeholder="address"
                value={student.address + ""}
                onChange={(e) => {
                  setStudent({ ...student, address: e.target.value });
                }}
                className="my-3 w-full"
                name="address"
              />

              <input
                className=" w-full rounded-md bg-[#34393d]  p-3"
                type="date"
                defaultValue={student.date_birth + ""}
                onChange={(e) => {
                  setStudent({
                    ...student,
                    date_birth: e.target.value,
                  });
                }}
              />
              <Select
                defaultValue={student.classId}
                onValueChange={(e) => {
                  console.log(e);
                  setStudent({
                    ...student,
                    classId: e,
                  });
                }}
              >
                <SelectTrigger className="my-3 w-full">
                  <SelectValue placeholder="Select a class" />
                </SelectTrigger>
                <SelectContent>
                  <SelectGroup>
                    {classs.map((c) => (
                      <SelectItem
                        key={c.id}
                        value={c.id}
                        className="my-3 w-full"
                      >
                        {c.name}
                      </SelectItem>
                    ))}
                  </SelectGroup>
                </SelectContent>
              </Select>
            </div>
          </AlertDialogDescription>
        </AlertDialogHeader>
        <AlertDialogFooter>
          <AlertDialogCancel>Cancel</AlertDialogCancel>
          <AlertDialogAction onClick={send}>Continue</AlertDialogAction>
        </AlertDialogFooter>
      </AlertDialogContent>
    </AlertDialog>
  );
  async function send() {
    const s: Student = { ...student };
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const ss: any = s;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    delete ss.id;

    const c = await fetch("/api/student", {
      method: "Post",
      body: JSON.stringify(ss),
    });
    console.log(c);
  }
}
