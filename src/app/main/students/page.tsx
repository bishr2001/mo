"use client";
import DataTableDemo from "./table";
import AddButton from "./addButton";
import AddManyButton from "./addManyButton";
import { useEffect, useState } from "react";
import { Student } from "~/server/db/schema";

export default function Students() {
  const [data, setData] = useState<Student[]>([]);
  useEffect(() => {
    fetch(`/api/student`, { cache: "no-store" })
      .then((response) => response.json())
      .catch((e) => {
        console.log(e);
      })
      .then((json: Student[]) => {
        setData(json);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);
  return (
    <div>
      <h1>Students</h1>
      <AddButton />
      <AddManyButton />
      <div>
        <DataTableDemo data={data}></DataTableDemo>{" "}
      </div>
    </div>
  );
}
