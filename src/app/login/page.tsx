import { getServerSession } from "next-auth";
import { redirect } from "next/navigation";
import LoginForm from "./form";

export default async function LoginPage() {
  const session = await getServerSession();

  if (session) {
    redirect("/main");
  }

  return (
    <section className="flex h-screen items-center justify-center bg-black">
      <div className="w-[600px]">
        <LoginForm />;
      </div>
    </section>
  );
}
