"use client";

import { signOut } from "next-auth/react";

const Logout = () => {
  return (
    <span
      className="cursor-pointer"
      onClick={async () => {
        await signOut();
      }}
    >
      Logout
    </span>
  );
};

export default Logout;
